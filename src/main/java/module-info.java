open module Main.app {

    requires javafx.fxml;
    requires javafx.controls;
    requires org.jetbrains.annotations;
    requires slf4j.api;
}
