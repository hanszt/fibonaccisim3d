package hzt.service;

import hzt.model.MovableCameraPlatform;
import hzt.model.fibonacci.FibonacciGroup;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public interface IAnimationService {

    void run(FibonacciGroup fibonacciGroup, MovableCameraPlatform cameraPlatform);

    void addLoopHandlersToTimelines(boolean start,
                                    EventHandler<ActionEvent> animationLoop, EventHandler<ActionEvent> statisticsLoop);
    void pauseAnimationTimeline();

    void startAnimationTimeline();
}
