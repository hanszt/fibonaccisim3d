package hzt.service;

import javafx.beans.property.SimpleStringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AboutService implements IAboutService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AboutService.class);
    private static final String RELATIVE_TEXT_RESOURCE_DIR = "../../about";

    public List<AboutText> loadContent() {
        List<AboutText> aboutTexts = new ArrayList<>();
       URL url = getClass().getResource(RELATIVE_TEXT_RESOURCE_DIR);
        if (url != null) {
            File fileDir = new File(url.getFile());
            if (fileDir.isDirectory()) {
                final var fileNames = Optional.ofNullable(fileDir.list())
                        .orElseThrow(() -> new IllegalStateException("No files in " + fileDir.getName()));
                for (String fileName : fileNames) {
                    String name = fileName.replace(".txt", "").replace("_", " ");
                    AboutText aboutText = new AboutText(name, new SimpleStringProperty(loadTextContent(fileName)));
                    aboutTexts.add(aboutText);
                }
            }
        } else {
            LOGGER.error("about folder not found at "+ RELATIVE_TEXT_RESOURCE_DIR + "...");
        }
        if (aboutTexts.isEmpty()) {
            aboutTexts.add(new AboutText("no content", new SimpleStringProperty()));
        }
        return aboutTexts;
    }

    private String loadTextContent(String fileName) {
        final var resource = getClass().getResource(RELATIVE_TEXT_RESOURCE_DIR + "/" + fileName);
        return Optional.ofNullable(resource)
                .map(URL::getFile)
                .stream()
                .map(AboutService::readInputFileByLine)
                .flatMap(Collection::stream)
                .collect(Collectors.joining(System.lineSeparator()));
    }

    private static List<String> readInputFileByLine(String path) {
        List<String> inputList = new ArrayList<>();
        File file = new File(path);
        try (Scanner input = new Scanner(file)) {
            while (input.hasNextLine()) {
                inputList.add(input.nextLine());
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File with path {} not found...", path, e);
        }
        return inputList;
    }


}
