package hzt.service;

import hzt.model.appearance.Resource;

import java.util.Set;

public interface IBackgroundService {

    Set<Resource> getResources();
}
