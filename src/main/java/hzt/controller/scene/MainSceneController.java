package hzt.controller.scene;

import hzt.controller.SceneManager;
import hzt.controller.sub_pane.AppearanceController;
import hzt.controller.sub_pane.StatisticsController;
import hzt.model.AnimationAttribute;
import hzt.model.AppConstants;
import hzt.model.MovableCameraPlatform;
import hzt.model.fibonacci.FibonacciGroup;
import hzt.service.AnimationService;
import hzt.service.IAnimationService;
import hzt.service.IMouseControlService;
import hzt.service.MouseControlService;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.io.IOException;
import java.time.LocalTime;

import static hzt.model.AppConstants.*;
import static hzt.model.AppConstants.Scene.ABOUT_SCENE;
import static hzt.model.AppConstants.Scene.MAIN_SCENE;

public class MainSceneController extends SceneController {

    @FXML
    private Tab appearanceTab;
    @FXML
    private Tab statisticsTab;
    @FXML
    private AnchorPane animationPane;
    @FXML
    private ComboBox<AnimationAttribute> attributeCombobox;
    @FXML
    private ToggleButton showAxisButton;
    @FXML
    private Slider distributionSlider;
    @FXML
    private Slider numberOfObjectsSlider;
    @FXML
    private Slider seedSizeSlider;
    @FXML
    private Slider turnFractionSlider;
    @FXML
    private Slider highlightingSlider;
    @FXML
    private Slider sizeSlider;
    @FXML
    private Slider animationIncrementSlider;
    @FXML
    private Slider cameraMaxVelocitySlider;
    @FXML
    private Slider cameraFieldOfViewSlider;

    private final Group subSceneRoot = new Group();
    private final Group fibonacciRoot = new Group();

    private final FibonacciGroup fibonacciGroup = new FibonacciGroup();
    private final IMouseControlService subSceneRootMouseControlService = new MouseControlService(fibonacciRoot);
    private final StatisticsController statisticsController = new StatisticsController();

    private final IAnimationService animationService = new AnimationService();

    private final SubScene subScene3D = new SubScene(subSceneRoot, 0, 0, true, SceneAntialiasing.BALANCED);
    private final MovableCameraPlatform cameraPlatform = new MovableCameraPlatform();

    public MainSceneController(SceneManager sceneManager) throws IOException {
        super(MAIN_SCENE.getFxmlFileName(), sceneManager);
        this.animationPane.getChildren().add(subScene3D);

        this.subSceneRoot.getChildren().add(fibonacciRoot);
        this.statisticsTab.setContent(statisticsController.getRoot());
    }

    @Override
    public void setup() {
        configureAnimationPane(animationPane);
        configureCameraPlatform(cameraPlatform);
        configureSubScene(subScene3D, animationPane);
        setInitControlValues();
        configureComboBoxes();
        configureSliders();
        configureAppearanceController();
        configureFibonacci(fibonacciGroup);
        subSceneRootMouseControlService.initMouseControl(animationPane);
        subSceneRootMouseControlService.setOrientation(INIT_ANGLE_X, INIT_ANGLE_Y);
        fibonacciGroup.getCoordinateSystem3D().visibleProperty().bind(showAxisButton.selectedProperty());
        fibonacciRoot.getChildren().addAll(fibonacciGroup.getSeeds(), fibonacciGroup.getCoordinateSystem3D());
        animationService.addLoopHandlersToTimelines(true, animationLoopEventHandler(), this::updateStatisticsLoop);
    }

    private void configureAppearanceController() {
        try {
            AppearanceController appearanceController = new AppearanceController(this, fibonacciGroup);
            this.appearanceTab.setContent(appearanceController.getRoot());
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void configureCameraPlatform(MovableCameraPlatform cameraPlatform) {
        cameraPlatform.setTranslate(INIT_CAMERA_PLATFORM_TRANSLATION);
        cameraPlatform.addKeyControls(scene);

        cameraPlatform.getCamera().fieldOfViewProperty().bindBidirectional(cameraFieldOfViewSlider.valueProperty());
        cameraPlatform.maxVelocityProperty().bindBidirectional(cameraMaxVelocitySlider.valueProperty());
    }

    private void configureSubScene(SubScene subScene, Pane animationPane) {
        subScene.widthProperty().bind(animationPane.widthProperty());
        subScene.heightProperty().bind(animationPane.heightProperty());
        subScene.setCamera(cameraPlatform.getCamera());
    }

    private void configureAnimationPane(AnchorPane animationPane) {
        animationPane.setPrefSize(INIT_ANIMATION_PANE_DIMENSION.getWidth(), INIT_ANIMATION_PANE_DIMENSION.getHeight());
        animationPane.setBackground(new Background(new BackgroundFill(INIT_BG_COLOR, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    private void configureComboBoxes() {
        attributeCombobox.getItems().addAll(turnFractionAttribute, highLightingAttribute, distributionAttribute, memberCountAttribute, none);
        attributeCombobox.setValue(turnFractionAttribute);
    }

    private void configureFibonacci(FibonacciGroup fibonacciGroup) {
        fibonacciGroup.angleFractionProperty().bind(turnFractionSlider.valueProperty());
        fibonacciGroup.distributionFactorProperty().bind(distributionSlider.valueProperty());
        fibonacciGroup.groupRadiusProperty().bind(sizeSlider.valueProperty());
        fibonacciGroup.highlightingValueProperty().bind(highlightingSlider.valueProperty());
        fibonacciGroup.seedSizeProperty().bind(seedSizeSlider.valueProperty());
        fibonacciGroup.memberSizeProperty().bind(numberOfObjectsSlider.valueProperty());
        fibonacciGroup.animationAttributeProperty().set(attributeCombobox.getValue());
        fibonacciGroup.getAnimationAttribute().updateIncrementSliderBounds(animationIncrementSlider);
    }

    private EventHandler<ActionEvent> animationLoopEventHandler() {
        return loop -> animationService.run(fibonacciGroup, cameraPlatform);
    }

    private void updateStatisticsLoop(ActionEvent loopEvent) {
        double animationIncrement = animationIncrementSlider.getValue() / INIT_FRAME_RATE;
        Duration runTimeSim = Duration.millis((LocalTime.now().toNanoOfDay() - startTimeScene.toNanoOfDay()) / 1e6);
        statisticsController.updateGroupStatistics(fibonacciGroup, subSceneRootMouseControlService);
        statisticsController.updateGlobalStatistics(runTimeSim, animationIncrement);
        statisticsController.updateCameraPlatformStatistics(cameraPlatform);
    }

    private void configureSliders() {
        turnFractionSlider.valueProperty().addListener((o,c,n) -> fibonacciGroup.updatePositions());
        distributionSlider.valueProperty().addListener((o, c, n) -> fibonacciGroup.updatePositions());
        sizeSlider.valueProperty().addListener((o, c, n) -> fibonacciGroup.updatePositions());
        highlightingSlider.valueProperty().addListener((o, c, n) -> fibonacciGroup.updateHighLighting());
    }

    private void setInitControlValues() {
        numberOfObjectsSlider.setValue(INIT_NUMBER_OF_SHAPES);
        seedSizeSlider.setValue(INIT_SEED_SIZE);
        distributionSlider.setValue(DISTRIBUTION_FACTOR);
        turnFractionSlider.setValue(GOLDEN_RATIO);
        sizeSlider.setValue(INIT_GROUP_RADIUS);
        highlightingSlider.setValue(INIT_FIBONACCI_NUMBER_INDEX);
        cameraFieldOfViewSlider.setValue(INIT_CAMERA_FOV);
        cameraMaxVelocitySlider.setValue(INIT_MAX_CAMERA_VELOCITY);
    }

    @FXML
    private void pauseSimButtonAction(ActionEvent actionEvent) {
        if (((ToggleButton) actionEvent.getSource()).isSelected()) {
            animationService.pauseAnimationTimeline();
        } else animationService.startAnimationTimeline();
    }

    @FXML
    private void resetButtonAction() {
        setInitControlValues();
        fibonacciGroup.reset();
        attributeCombobox.getItems().forEach(AnimationAttribute::reset);
        animationIncrementSlider.setValue(attributeCombobox.getValue().getCurValue());
    }

    @FXML
    private void distributionButtonAction() {
        distributionSlider.setValue(DISTRIBUTION_FACTOR);
    }

    @FXML
    private void goldenRatioButtonAction() {
        turnFractionSlider.setValue(GOLDEN_RATIO);
    }

    @FXML
    public void recenterButtonAction() {
        cameraPlatform.setTranslate(INIT_CAMERA_PLATFORM_TRANSLATION);
        cameraPlatform.getCamera().setFieldOfView(INIT_CAMERA_FOV);
        subSceneRootMouseControlService.setOrientation(INIT_ANGLE_X, INIT_ANGLE_Y);
        subSceneRootMouseControlService.setTargetTranslation(Point3D.ZERO);
    }

    @FXML
    private void openBrowserButtonAction(ActionEvent actionEvent) {
        Application application = new Application() {
            @Override
            public void start(Stage stage) {
                //This new instance is to refer to the browser
            }
        };
        application.getHostServices().showDocument(AppConstants.getProperty("my_repo", "https://gitlab.com/hanszt"));
        actionEvent.consume();
    }

    @FXML
    private void animatedAttributeComboboxAction() {
        fibonacciGroup.animationAttributeProperty().setValue(attributeCombobox.getValue());
        attributeCombobox.getValue().updateIncrementSliderBounds(animationIncrementSlider);
    }

    @FXML
    private void showAbout() {
        sceneManager.setupScene(ABOUT_SCENE);
    }

    protected SceneController getController() {
        return this;
    }

    private final AnimationAttribute none = new AnimationAttribute(
            "None", 0, 0, 0) {
        @Override
        public void updateSliderValue() {
            //no attribute selected...
        }
    };

    private final AnimationAttribute turnFractionAttribute = new AnimationAttribute(
            "Turn fraction", -1e-5, 1e-5, 1e-7) {
        @Override
        public void updateSliderValue() {
            turnFractionSlider.setValue(turnFractionSlider.getValue() + animationIncrementSlider.getValue());
            setCurValue(animationIncrementSlider.getValue());
        }
    };

    private final AnimationAttribute distributionAttribute = new AnimationAttribute(
            "Distribution", -.01, .01, -0.005) {
        @Override
        public void updateSliderValue() {
            distributionSlider.setValue(distributionSlider.getValue() + animationIncrementSlider.getValue());
            setCurValue(animationIncrementSlider.getValue());
        }
    };

    private final AnimationAttribute highLightingAttribute = new AnimationAttribute(
            "Highlighting", -.1, .1, .05) {
        @Override
        public void updateSliderValue() {
            highlightingSlider.setValue(highlightingSlider.getValue() + animationIncrementSlider.getValue());
            setCurValue(animationIncrementSlider.getValue());
        }
    };

    private final AnimationAttribute memberCountAttribute = new AnimationAttribute(
            "Member count", -10, 10, 1) {
        @Override
        public void updateSliderValue() {
            numberOfObjectsSlider.setValue(numberOfObjectsSlider.getValue() + animationIncrementSlider.getValue());
            setCurValue(animationIncrementSlider.getValue());
        }
    };

    public AnchorPane getAnimationPane() {
        return animationPane;
    }

}
