package hzt.model;

import javafx.scene.control.Slider;

public abstract class AnimationAttribute {

    private final String name;
    private final double minValue;
    private final double maxValue;
    private final double initValue;
    private double curValue;

    protected AnimationAttribute(String name, double minValue, double maxValue, double initValue) {
        this.name = name;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.initValue = initValue;
        this.curValue = initValue;
    }

    public abstract void updateSliderValue();

    // first set the min and max value, after that the current value
    public void updateIncrementSliderBounds(Slider slider) {
        slider.setMin(minValue);
        slider.setMax(maxValue);
        slider.setValue(curValue);
    }

    public void setCurValue(double curValue) {
        this.curValue = curValue;
    }

    public double getCurValue() {
        return curValue;
    }

    public void reset() {
        curValue = initValue;
    }

    public String toString() {
        return name;
    }

}
