package hzt.model;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;

public class SmartShape extends Group {

    private final Shape3D body;
    private final Color color;
    private final PhongMaterial material = new PhongMaterial();

    public SmartShape(Shape3D body, Color color) {
        this.body = body;
        this.color = color;
        configureComponents();
        getChildren().add(body);
    }

    private void configureComponents() {
        body.setMaterial(material);
    }

    public PhongMaterial getMaterial() {
        return material;
    }

    public Shape3D getBody() {
        return body;
    }

    public Color getColor() {
        return color;
    }

}
