package hzt.model.fibonacci;

import hzt.model.AnimationAttribute;
import hzt.model.CoordinateSystem3D;
import hzt.model.CylinderCoordinateSystem3D;
import hzt.model.SmartShape;
import javafx.beans.property.*;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Sphere;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public class FibonacciGroup implements Iterable<SmartShape> {

    private final Group seeds = new Group();
    private final CoordinateSystem3D coordinateSystem3D = new CylinderCoordinateSystem3D();

    private final IntegerProperty memberSize = new SimpleIntegerProperty();
    private final IntegerProperty highlightingValue = new SimpleIntegerProperty();

    private final DoubleProperty angleFraction = new SimpleDoubleProperty();
    private final DoubleProperty distributionFactor = new SimpleDoubleProperty();
    private final DoubleProperty seedSize = new SimpleDoubleProperty();
    private final DoubleProperty groupRadius = new SimpleDoubleProperty();

    private final ObjectProperty<Color> seedColor = new SimpleObjectProperty<>();
    private final ObjectProperty<Color> highlightingColor = new SimpleObjectProperty<>();
    private final ObjectProperty<AnimationAttribute> animationAttribute = new SimpleObjectProperty<>();

    public FibonacciGroup() {
        super();
        memberSize.addListener((o, c, n) -> controlMembersSize(n.intValue()));
        seeds.getChildren().addListener(this::onSeedSizeChanged);
    }

    private void onSeedSizeChanged(ListChangeListener.Change<? extends Node> change) {
        int index = 0;
        for (Node node : change.getList()) {
            setHighlighting((SmartShape) node, index);
            index++;
        }
    }

    public void controlMembersSize(int numberOfBalls) {
        while (seeds.getChildren().size() != numberOfBalls) {
            if (seeds.getChildren().size() < numberOfBalls) addSmartShapeToGroup();
            else removeShape();
        }
        updatePositions();
    }

    private void addSmartShapeToGroup() {
        SmartShape shape = createSmartShape(seedSize, seedColor.get());
        seeds.getChildren().add(shape);
    }

    private SmartShape createSmartShape(DoubleProperty shapeSize, Color color) {
        Sphere sphere = new Sphere();
        sphere.radiusProperty().bind(shapeSize);
        return new SmartShape(sphere, color);
    }

    public void updatePositions() {
        int counter = 0;
        for (SmartShape child : this) {
            setPosition(child, counter, getSeeds().getChildren().size());
            counter++;
        }
    }

    public void updateHighLighting() {
        int index = 0;
        for (SmartShape child : this) {
            setHighlighting(child, index);
            index++;
        }
    }

    public void loopUpdate() {
        animationAttribute.get().updateSliderValue();
    }

    private void setHighlighting(SmartShape child, int counter) {
        if (highlightingValue.get() != 0) {
            child.getMaterial().setDiffuseColor(counter % highlightingValue.get() == 0 ? highlightingColor.get() : seedColor.get());
        } else {
            child.getMaterial().setDiffuseColor(seedColor.get());
        }
    }

    public void setPosition(SmartShape shape, int index, int numberOfBalls) {
        double normalizedIndex = (double) index / (numberOfBalls - 1);
        double radius = Math.pow(normalizedIndex, distributionFactor.get()) * groupRadius.get();
        double theta = 2 * Math.PI * angleFraction.get() * index;
        double phi = Math.acos(1 - (2 * normalizedIndex));
        double x = radius * Math.cos(theta) * Math.sin(phi);
        double y = radius * Math.sin(theta) * Math.sin(phi);
        double z = radius * (1 - (2 * normalizedIndex));
        shape.setTranslateX(x);
        shape.setTranslateY(y);
        shape.setTranslateZ(z);
    }

    private void removeShape() {
        ObservableList<Node> list = seeds.getChildren();
        SmartShape shape = (SmartShape) list.get(0);
        seeds.getChildren().remove(shape);
    }

    public void reset() {
    }

    @NotNull
    @Override
    public Iterator<SmartShape> iterator() {
        return seeds.getChildren().stream().map(n -> (SmartShape) n).iterator();
    }

    public IntegerProperty memberSizeProperty() {
        return memberSize;
    }

    public double getAngleFraction() {
        return angleFraction.get();
    }

    public DoubleProperty angleFractionProperty() {
        return angleFraction;
    }

    public double getDistributionFactor() {
        return distributionFactor.get();
    }

    public DoubleProperty distributionFactorProperty() {
        return distributionFactor;
    }

    public DoubleProperty groupRadiusProperty() {
        return groupRadius;
    }

    public DoubleProperty seedSizeProperty() {
        return seedSize;
    }

    public ObjectProperty<Color> seedColorProperty() {
        return seedColor;
    }

    public ObjectProperty<Color> highlightingColorProperty() {
        return highlightingColor;
    }

    public ObjectProperty<AnimationAttribute> animationAttributeProperty() {
        return animationAttribute;
    }

    public AnimationAttribute getAnimationAttribute() {
        return animationAttribute.get();
    }

    public int getHighlightingValue() {
        return highlightingValue.get();
    }

    public IntegerProperty highlightingValueProperty() {
        return highlightingValue;
    }

    public Group getSeeds() {
        return seeds;
    }

    public CoordinateSystem3D getCoordinateSystem3D() {
        return coordinateSystem3D;
    }
}
